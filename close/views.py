from django.shortcuts import render_to_response
from forms import EssaiForm
from django.template import RequestContext
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from serializers import UserSerializer, GroupSerializer

# Create your views here.
def index(request):
    context={
        'form': EssaiForm(),
    }
    return render_to_response('close/index.html',
                              context,
                              context_instance=RequestContext(request))
class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
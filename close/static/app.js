/**
 * Created by catalia on 06/03/16.
 */

var app = angular.module('App', ['ngRoute'])
app.config(function($interpolateProvider, $routeProvider){
    $interpolateProvider.startSymbol('[[').endSymbol(']]',
    $routeProvider
        .when('/',{templateUrl :  'partials/home.html'})
        .when('/comments', {templateUrl :  'static/partials/comments.html'})
        .otherwise({redirectTo : '/'})
    );
});
app.controller('homectrl', function($scope){
    $scope.comments=[

  {
    "username": "kokoi",
    "city": "lome",
    "email": "jiokjj@gjk.vom",
    "content": "Anim id velit dolore mollit ad in. Esse anim officia nulla aliqua pariatur adipisicing occaecat do ullamco laborum. Do consequat cupidatat sunt ad velit nulla. Do nostrud sint aliqua laboris officia laborum officia cillum nostrud ut amet minim eu aliqua. Incididunt aliquip deserunt nulla pariatur nisi elit cillum ad minim laboris eu."
  },
  {
    "username": "komi",
    "city": "agoe",
    "email": "koei@gob.bol",
    "content": "Eu nulla quis laborum amet id dolor culpa commodo deserunt. Et qui nisi amet ea est reprehenderit proident occaecat excepteur enim sunt. Consequat reprehenderit Lorem amet non fugiat anim laboris. Esse officia Lorem dolor reprehenderit labore nisi commodo irure ex adipisicing laboris. Ex aliquip dolore commodo eu."
  },
  {
    "username": "koujk",
    "city": "babi",
    "email": "toto@titi.com",
    "content": "Cillum deserunt sint aute veniam aute in sit ut ad id commodo. Qui nisi laboris et do. Ex duis non tempor ut est id ea nisi. Lorem deserunt deserunt et aute occaecat irure aliqua cillum proident nulla. Exercitation est tempor cupidatat exercitation irure minim sint consequat minim commodo."
  },
  {
    "username": "kojhn",
    "city": "kumasi",
    "email": "uio@ui.com",
    "content": "Do dolor aute sint enim dolore. Irure laboris aute pariatur dolor voluptate laboris excepteur esse magna. Voluptate aliqua ea ipsum nulla tempor laborum ullamco ullamco eu ea sit sunt irure. Enim fugiat ex et officia cillum id culpa cupidatat ad amet. Adipisicing nulla sit consectetur irure nulla enim."
  },
  {
    "username": "jnkll",
    "city": "kpome",
    "email": "not@tyu.com",
    "content": "Nulla ullamco laboris amet irure qui laboris mollit laborum excepteur elit. Ut culpa ullamco ut nostrud quis sunt officia cillum occaecat nisi. Enim enim laborum commodo enim incididunt cupidatat quis ad. Irure ex est non elit anim. Occaecat sunt ipsum sunt voluptate laboris consectetur excepteur laborum eu magna minim et Lorem."
  }
];
});

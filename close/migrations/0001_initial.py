# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.contrib.auth.models


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='Profil',
            fields=[
                ('user', models.OneToOneField(primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('addresse', models.TextField()),
                ('telephone', models.IntegerField()),
                ('biographie', models.TextField()),
                ('premium', models.CharField(default=b'A', max_length=1)),
                ('facebook', models.CharField(max_length=256, blank=True)),
                ('twitter', models.CharField(max_length=256, blank=True)),
                ('linkedin', models.CharField(max_length=256, blank=True)),
                ('photo', models.ImageField(upload_to=b'photo', blank=True)),
                ('active', models.CharField(max_length=4, null=True)),
                ('idcard', models.ImageField(upload_to=b'idcard', blank=True)),
                ('slug', models.CharField(max_length=256)),
            ],
            options={
                'abstract': False,
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
            },
            bases=('auth.user',),
            managers=[
                (b'objects', django.contrib.auth.models.UserManager()),
            ],
        ),
    ]

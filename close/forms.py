from django import forms
from models import essai

class EssaiForm(forms.ModelForm):
    class Meta:
        model=essai
        fields=('art',)

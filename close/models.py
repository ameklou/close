from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Profil(User):
    """ class contenant les informations sur l'utilisateur"""
    user=models.OneToOneField(User)
    addresse=models.TextField()
    telephone=models.IntegerField()
    biographie=models.TextField()
    premium=models.CharField(max_length=1, default="A")
    facebook=models.CharField(max_length=256,blank=True)
    twitter=models.CharField(max_length=256,blank=True)
    linkedin=models.CharField(max_length=256, blank=True)
    photo=models.ImageField(upload_to="photo", blank=True)
    active=models.CharField(max_length=4,null=True)
    idcard=models.ImageField(upload_to="idcard", blank=True)
    slug=models.CharField(max_length=256)

    def __str__(self):
        return self.user.username

class essai(models.Model):
    art=models.CharField(max_length=256)

